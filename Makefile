CC = gcc
CFLAGS = -Wall -Wextra -Werror -pedantic -O3
LIBS = -lncurses

OBJ = \
 main.o \
 util.o

ntt: $(OBJ)
		$(CC) $(CFLAGS) -o ntt $(OBJ) $(LIBS)

main.o: util.o

util.o: util.h

.PHONY: clean
clean:
		rm -f ntt $(OBJ)
