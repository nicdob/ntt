#include <curses.h>
#include <stdio.h>

#include "util.h"

// Colors
#define DEFAULT_CHAR 1
#define WRONG_CHAR   2
#define RIGHT_CHAR   3


// TODO: selection of text files, pick random
int showRandomText()
{
    FILE *fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    int read_length = 0;

    fp = fopen("data/text.txt", "r");

    if (!fp)
        return 1;

    attron(COLOR_PAIR(DEFAULT_CHAR));
    while ((read = getline(&line, &len, fp)) != -1) {
        printw("%s", line);
        read_length += read;
    }
    attroff(COLOR_PAIR(DEFAULT_CHAR));

    fclose(fp);
    return read_length;
}

int main()
{
    uint64_t start_t, stop_t;
    uint64_t time_diff;

    // TODO: start time on first input
    start_t = get_posix_clock_time();

    int x = 0;
    int y = 0;

    initscr();
    noecho();
    curs_set(1);

    if (!has_colors()) {
        endwin();
        printf("Your Terminal does not support color\n");
        return 1;
    }

    start_color();
    use_default_colors();
    init_pair(DEFAULT_CHAR, -1, -1);
    init_pair(RIGHT_CHAR, COLOR_GREEN, -1);
    init_pair(WRONG_CHAR, -1, COLOR_RED);


    /* if(showRandomText()) */
    /*     printw("%s", "Error reading textfile"); */
    int read_length = showRandomText();

    move(0,0);
    refresh();

    while (true)
    {
        /* mvprintw(y, x, "%c", input); */
        /* refresh(); */
        /* move(y,x); */
        char input = getch();
        char ch = inch() & A_CHARTEXT;
        int char_color = COLOR_PAIR(DEFAULT_CHAR);

        if (input != ch) {
            char_color = COLOR_PAIR(WRONG_CHAR);
        } else
            char_color = COLOR_PAIR(RIGHT_CHAR);

        attron(char_color);
        printw("%c", ch);
        attroff(char_color);

        x++;
        move(y, x);
        refresh();

        if (x == (read_length - 1)) {
            printw("\n");
            move(y+1, 0);
            break;
        }
    }

    stop_t = get_posix_clock_time();

    time_diff = stop_t - start_t;

    /* Convert Micro- to Seconds */
    double time_diff_seconds = (double)time_diff / 1000000;

    printw("Time: %.2f\n", time_diff_seconds);

    getch();

    endwin();
}
