#ifndef UTIL_H_
#define UTIL_H_
#include <stdint.h>

uint64_t get_posix_clock_time();

#endif // UTIL_H_
